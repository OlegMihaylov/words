package ru.mihaylov.words.ui

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.no_data_view.view.*
import ru.mihaylov.words.R

/**
 * Компонент для отображения блока "Нет данных"
 */
class NoDataView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : LinearLayout(context, attrs, defStyleAttr) {

    init {
        View.inflate(context, R.layout.no_data_view, this)
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.NoDataView)
        val imageDrawable = attributes.getDrawable(R.styleable.NoDataView_image)
        if (imageDrawable != null) {
            no_data_icon.setImageDrawable(imageDrawable)
        }
        val title = attributes.getString(R.styleable.NoDataView_title)
        if (title != null) {
            no_data_text.text = title
        }
        attributes.recycle()
    }

}