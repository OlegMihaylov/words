package ru.mihaylov.words.ui.common

import android.widget.ImageView
import com.bumptech.glide.Glide

/**
 * Отобразить изображение [url] в текущем ImageView
 */
fun ImageView.loadByUrl(url: String?) {
    Glide
        .with(this.context)
        .load(url)
        .centerCrop()
        .into(this)
}

/**
 * Отобразить изображение [url] в текущем ImageView
 * Обрезаем в форме круга
 */
fun ImageView.loadCircleByUrl(url: String?) {
    Glide
        .with(this.context)
        .load(url)
        .centerCrop()
        .circleCrop()
        .into(this)
}