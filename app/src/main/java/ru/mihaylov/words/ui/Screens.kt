package ru.mihaylov.words.ui

import ru.mihaylov.words.ui.details.MeaningDetailsFragment
import ru.mihaylov.words.ui.search.SearchFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {

    object SearchScreen : SupportAppScreen() {
        override fun getFragment() = SearchFragment()
    }

    class MeaningDetailsScreen(private val id: Long) : SupportAppScreen() {
        override fun getFragment() = MeaningDetailsFragment.getInstance(id)
    }

}