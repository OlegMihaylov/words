package ru.mihaylov.words.ui.search

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import ru.mihaylov.words.presentation.search.MeaningListItem
import ru.mihaylov.words.presentation.search.MultiMeaningListItem
import ru.mihaylov.words.presentation.search.SingleMeaningListItem

/**
 * Адаптер для отображения списка слов из справочника
 */
class WordListAdapter(private val wordClickListener: (MultiMeaningListItem) -> Unit,
                      private val meaningClickListener: (SingleMeaningListItem) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val itemList = mutableListOf<MeaningListItem>()

    /**
     * Установить список значений слов [meaningItemList] для отображения
     */
    fun setData(meaningItemList: List<MeaningListItem>) {
        val oldData = itemList.toList()

        itemList.clear()
        itemList.addAll(meaningItemList)

        DiffUtil.calculateDiff(DiffCallback(itemList, oldData), false)
            .dispatchUpdatesTo(this)
    }

    override fun getItemViewType(position: Int): Int {
        return when (itemList[position]) {
            is SingleMeaningListItem -> SINGLE_MEANING_LIST_ITEM
            is MultiMeaningListItem -> MULTI_MEANING_LIST_ITEM
            else -> throw RuntimeException("Неизвестный тип данных в списке")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : RecyclerView.ViewHolder {
        return when (viewType) {
            SINGLE_MEANING_LIST_ITEM ->  SingleMeaningItemViewHolder.from(parent, meaningClickListener)
            MULTI_MEANING_LIST_ITEM -> MultiMeaningItemViewHolder.from(parent, wordClickListener)
            else -> throw RuntimeException("Неизвестный тип данных в списке")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (val item = itemList[position]) {
            is SingleMeaningListItem -> (holder as SingleMeaningItemViewHolder).bind(item)
            is MultiMeaningListItem -> (holder as MultiMeaningItemViewHolder).bind(item)
            else -> throw RuntimeException("Неизвестный тип данных в списке")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any>) {
        val item = itemList[position]
        if (payloads.contains(ITEM_CHANGED_PAYLOAD) && item is MultiMeaningListItem) {
            (holder as MultiMeaningItemViewHolder).update(item)
        } else {
            onBindViewHolder(holder, position)
        }
    }

    override fun getItemCount() = itemList.size

    private class DiffCallback(
        private val newItems: List<MeaningListItem>,
        private val oldItems: List<MeaningListItem>
    ) : DiffUtil.Callback() {

        override fun getOldListSize() = oldItems.size
        override fun getNewListSize() = newItems.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldItems[oldItemPosition]
            val newItem = newItems[newItemPosition]

            return if (newItem is SingleMeaningListItem && oldItem is SingleMeaningListItem) {
                newItem.id == oldItem.id
            } else if (newItem is MultiMeaningListItem && oldItem is MultiMeaningListItem) {
                newItem.id == oldItem.id
            } else {
                false
            }
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldItems[oldItemPosition]
            val newItem = newItems[newItemPosition]

            return if (newItem is SingleMeaningListItem && oldItem is SingleMeaningListItem) {
                newItem == oldItem
            } else if (newItem is MultiMeaningListItem && oldItem is MultiMeaningListItem) {
                newItem == oldItem
            } else {
                false
            }
        }

        override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int) = ITEM_CHANGED_PAYLOAD
    }

    companion object {
        private const val SINGLE_MEANING_LIST_ITEM = 1
        private const val MULTI_MEANING_LIST_ITEM = 2

        private const val ITEM_CHANGED_PAYLOAD = 1
    }

}
