package ru.mihaylov.words.ui.search

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.core.widget.doAfterTextChanged
import kotlinx.android.synthetic.main.search_fragment.*
import moxy.MvpAppCompatFragment
import moxy.ktx.moxyPresenter
import ru.mihaylov.words.R
import ru.mihaylov.words.presentation.search.MeaningListItem
import ru.mihaylov.words.presentation.search.SearchPresenter
import ru.mihaylov.words.presentation.search.SearchView

/**
 * Фрагмент поиска слова
 */
class SearchFragment : MvpAppCompatFragment(R.layout.search_fragment), SearchView {

    private val presenter by moxyPresenter { SearchPresenter() }
    private lateinit var wordListAdapter: WordListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        word_input.doAfterTextChanged {
            presenter.onSearchSubmit(it.toString())
        }
        wordListAdapter = WordListAdapter(wordClickListener = presenter::onMultiItemClick,
                                        meaningClickListener = presenter::onSingleItemClick)
        result_list.adapter = wordListAdapter
    }

    override fun showSearchResults(meaningList: List<MeaningListItem>) {
        manageBlocks(result_list.id)
        wordListAdapter.setData(meaningList)
    }

    override fun showEmptyResult() {
        manageBlocks(search_empty_block.id)
    }

    override fun showStartSearchBlock() {
        manageBlocks(search_start_block.id)
    }

    override fun showErrorScreen() {
        manageBlocks(search_error_block.id)
    }

    private fun manageBlocks(@LayoutRes blockId: Int) {
        search_start_block.visibility = if (search_start_block.id == blockId) View.VISIBLE else View.GONE
        search_empty_block.visibility = if (search_empty_block.id == blockId) View.VISIBLE else View.GONE
        search_error_block.visibility = if (search_error_block.id == blockId) View.VISIBLE else View.GONE
        result_list.visibility = if (result_list.id == blockId) View.VISIBLE else View.GONE
    }

}
