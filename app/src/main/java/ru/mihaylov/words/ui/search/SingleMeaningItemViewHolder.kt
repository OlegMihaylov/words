package ru.mihaylov.words.ui.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.single_meaning_list_item.view.*

import ru.mihaylov.words.R
import ru.mihaylov.words.presentation.search.SingleMeaningListItem
import ru.mihaylov.words.ui.common.loadCircleByUrl

/**
 * Вью холдер для отображения слова с одним значением
 */
class SingleMeaningItemViewHolder(
    view: View,
    clickListener: (SingleMeaningListItem) -> Unit
): RecyclerView.ViewHolder(view) {

    private val wordTextView = view.word_text
    private val meaningsTextView = view.meanings_text
    private val imageView = view.image

    private lateinit var currentItem: SingleMeaningListItem

    init {
        itemView.setOnClickListener { clickListener(currentItem) }
    }

    /**
     * Связать вью с отображаемой записью [item]
     */
    fun bind(item: SingleMeaningListItem) {
        currentItem = item
        wordTextView.text = item.text
        meaningsTextView.text = item.meaning
        imageView.loadCircleByUrl(item.imageUrl)
    }

    companion object {

        fun from(parent: ViewGroup,
                 clickListener: (SingleMeaningListItem) -> Unit
        ): SingleMeaningItemViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val view = layoutInflater.inflate(R.layout.single_meaning_list_item, parent, false)
            return SingleMeaningItemViewHolder(view, clickListener)
        }

    }

}