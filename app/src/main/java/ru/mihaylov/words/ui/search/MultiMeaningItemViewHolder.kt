package ru.mihaylov.words.ui.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.multi_meaning_list_item.view.*
import ru.mihaylov.words.R
import ru.mihaylov.words.presentation.search.MultiMeaningListItem

/**
 * Вью холдер для отображения слова с несколькими значениями
 */
class MultiMeaningItemViewHolder(
    view: View,
    private val clickListener: (MultiMeaningListItem) -> Unit
): RecyclerView.ViewHolder(view) {

    private val meaningsCountTextView = view.word_meanings_count
    private val wordTextView = view.word_text
    private val meaningsTextView = view.meanings_text
    private val expandImage = view.expand_image

    private lateinit var currentItem: MultiMeaningListItem

    init {
        itemView.setOnClickListener { clickListener(currentItem) }
    }

    /**
     * Связать вью с отображаемой записью [item]
     */
    fun bind(item: MultiMeaningListItem) {
        currentItem = item
        meaningsCountTextView.text = "${item.wordCount}"
        wordTextView.text = item.text
        meaningsTextView.text = item.meanings
        if (item.isExpanded) {
            expandImage(false)
        } else {
            collapseImage(false)
        }
    }

    /**
     * Обновить отображение записи [item]
     */
    fun update(item: MultiMeaningListItem) {
        if (item.isExpanded) {
            expandImage(true)
        } else {
            collapseImage(true)
        }
    }

    private fun expandImage(animated: Boolean) {
        if (animated) {
            expandImage.animate().setDuration(ROTATION_DURATION).rotation(EXPANDED_ANGLE)
        } else {
            expandImage.rotation = EXPANDED_ANGLE
        }
    }

    private fun collapseImage(animated: Boolean) {
        if (animated) {
            expandImage.animate().setDuration(ROTATION_DURATION).rotation(COLLAPSED_ANGLE)
        } else {
            expandImage.rotation = COLLAPSED_ANGLE
        }
    }

    companion object {

        private const val ROTATION_DURATION = 300L
        private const val COLLAPSED_ANGLE = 0f
        private const val EXPANDED_ANGLE = 90f

        fun from(parent: ViewGroup,
                 clickListener: (MultiMeaningListItem) -> Unit
        ): MultiMeaningItemViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val view = layoutInflater.inflate(R.layout.multi_meaning_list_item, parent, false)
            return MultiMeaningItemViewHolder(view, clickListener)
        }

    }

}