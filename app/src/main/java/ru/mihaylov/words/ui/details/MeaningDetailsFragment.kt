package ru.mihaylov.words.ui.details

import android.os.Bundle
import kotlinx.android.synthetic.main.defininition_example_item.view.*
import kotlinx.android.synthetic.main.meaining_details_fragment.*
import moxy.MvpAppCompatFragment
import moxy.ktx.moxyPresenter
import ru.mihaylov.words.R
import ru.mihaylov.words.entity.MeaningDetails
import ru.mihaylov.words.presentation.details.MeaningDetailPresenter
import ru.mihaylov.words.presentation.details.MeaningDetailView
import ru.mihaylov.words.ui.common.loadByUrl

class MeaningDetailsFragment : MvpAppCompatFragment(R.layout.meaining_details_fragment), MeaningDetailView {

    private val presenter by moxyPresenter {
        val id = requireArguments().getLong(MEANING_ID_KEY)
        MeaningDetailPresenter(id)
    }

    override fun showMeaningDetails(meaningDetails: MeaningDetails) {
        with(meaningDetails) {
            meaning_image.loadByUrl(imageList.first())
            meaning_text.text = text
            prefix?.let {
                meaning_text.text = "${prefix.orEmpty()} ${text}"
            }
            translation_text.text = translation.text
            transcription_text.text = "/${transcription}/"
            definition_text.text = definition.text
            examples_container.removeAllViews()
            examples.forEach {
                val exampleView = layoutInflater.inflate(R.layout.defininition_example_item, examples_container, false)
                exampleView.example_definition_text.text = it.text
                examples_container.addView(exampleView)
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        toolbar.setNavigationOnClickListener { presenter.onBackPressed() }
    }

    companion object {
        private const val MEANING_ID_KEY = "MEANING_ID"

        fun getInstance(id: Long): MeaningDetailsFragment {
            return MeaningDetailsFragment().apply {
                arguments = Bundle().apply {
                    putLong(MEANING_ID_KEY, id)
                }
            }
        }

    }

}