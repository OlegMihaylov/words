package ru.mihaylov.words.model.data.server

import com.google.gson.annotations.SerializedName
import ru.mihaylov.words.entity.MeaningDetails

/**
 * Рест представление детальной информации о значении слова
 */
data class RestMeaningDetails(
    @SerializedName("id") val id: String,
    @SerializedName("prefix") val prefix: String?,
    @SerializedName("text") val text: String,
    @SerializedName("translation") val translation: RestTranslation,
    @SerializedName("transcription") val transcription: String,
    @SerializedName("images") val images: List<RestImage>,
    @SerializedName("difficultyLevel") val difficultyLevel: Long?,
    @SerializedName("partOfSpeechCode") val partOfSpeechCode: String,
    @SerializedName("definition") val definition: RestDefinition,
    @SerializedName("examples") val examples: List<RestDefinition>
) {
    fun toEntity(): MeaningDetails {
        return MeaningDetails(
            id = id,
            prefix = prefix,
            text = text,
            translation = translation.toEntity(),
            transcription = transcription,
            imageList = images.map { it.url.wrapUrl().removeUrlParameters() },
            definition = definition.toEntity(),
            examples = examples.map { it.toEntity() }
        )
    }
}

data class RestImage(
    @SerializedName("url") val url: String
)