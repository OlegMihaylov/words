package ru.mihaylov.words.model.data.server

import com.google.gson.annotations.SerializedName
import ru.mihaylov.words.entity.Translation

/**
 * Рест представление перевода
 */
data class RestTranslation(
    @SerializedName("text") val text: String,
    @SerializedName("note") val note: String?
) {
    fun toEntity() = Translation(
        text = text,
        note = note
    )
}