package ru.mihaylov.words.model.usecase.search

import ru.mihaylov.words.entity.Word

/**
 * Сценарий поиска слова
 */
interface SearchWordUseCase {

    /**
     * Выполнить поиск слова [search] в словаре
     */
    suspend fun searchWord(search: String) : List<Word>
}