package ru.mihaylov.words.model.data.server

import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Интерфейс доступа к справочнику через Api
 */
interface DictionaryApi {

    @GET("v1/words/search")
    suspend fun searchWord(@Query("search") word: String) : List<RestWord>

    @GET("v1/meanings")
    suspend fun getMeaningDetails(@Query("ids") id: Long) : List<RestMeaningDetails>

}