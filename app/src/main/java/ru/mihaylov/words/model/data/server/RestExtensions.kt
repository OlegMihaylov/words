package ru.mihaylov.words.model.data.server

import android.net.Uri

/**
 * Добавить протокол к url адресу
 */
fun String.wrapUrl() = "https:$this"

/**
 * Удалить параметры из url адреса
 */
fun String.removeUrlParameters() = this.removeSuffix(Uri.parse(this).query ?: "")

