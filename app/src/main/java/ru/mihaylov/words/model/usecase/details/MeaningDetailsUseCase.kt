package ru.mihaylov.words.model.usecase.details

import ru.mihaylov.words.entity.MeaningDetails

/**
 * Сценарий получения детальной информации о значении слова
 */
interface MeaningDetailsUseCase {

    /**
     * Получить детальную информацию о значении слова
     * по идентификатору [id]
     */
    suspend fun getMeaningDetails(id: Long): MeaningDetails

}