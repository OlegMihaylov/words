package ru.mihaylov.words.model.data.server

import com.google.gson.annotations.SerializedName
import ru.mihaylov.words.entity.Word

/**
 * Рест представление слова в словаре
 */
data class RestWord(
    @SerializedName("id") val id: Long,
    @SerializedName("text") val text: String,
    @SerializedName("meanings") val meanings: List<RestMeaning>
) {
    fun toEntity() = Word(
        id = id,
        text = text,
        meanings = meanings.mapNotNull { it.toEntity() }
    )
}