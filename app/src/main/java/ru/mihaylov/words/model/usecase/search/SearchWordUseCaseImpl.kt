package ru.mihaylov.words.model.usecase.search

import ru.mihaylov.words.entity.Word
import ru.mihaylov.words.model.repository.words.WordRepository

/**
 * Реализация сценария поиска слова в словаре
 */
class SearchWordUseCaseImpl(private val wordRepository: WordRepository) : SearchWordUseCase {

    override suspend fun searchWord(search: String): List<Word> {
        return wordRepository.searchWord(search)
    }

}