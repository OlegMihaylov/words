package ru.mihaylov.words.model.usecase.details

import ru.mihaylov.words.entity.MeaningDetails
import ru.mihaylov.words.model.repository.words.WordRepository

/**
 *  Реализация сценария получения детальной информации о значении слова
 */
class MeaningDetailsUseCaseImpl(private val wordRepository: WordRepository) : MeaningDetailsUseCase {

    override suspend fun getMeaningDetails(id: Long): MeaningDetails {
        return wordRepository.getMeaningDetails(id)
    }

}