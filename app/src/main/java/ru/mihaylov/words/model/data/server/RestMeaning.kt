package ru.mihaylov.words.model.data.server

import com.google.gson.annotations.SerializedName
import ru.mihaylov.words.entity.Meaning
import ru.mihaylov.words.entity.PartOfSpeech

/**
 * Рест представление значения слова
 */
data class RestMeaning(
    @SerializedName("id") val id: Long,
    @SerializedName("partOfSpeechCode") val partOfSpeechCode: String,
    @SerializedName("translation") val translation: RestTranslation,
    @SerializedName("previewUrl") val previewUrl: String?,
    @SerializedName("imageUrl") val imageUrl: String?,
    @SerializedName("transcription") val transcription: String,
    @SerializedName("soundUrl") val soundUrl: String?
) {
    /**
     * Преобразовать рест представление к бизнес представлению
     */
    fun toEntity() : Meaning? {
        val partOfSpeech = partOfSpeechByCode(partOfSpeechCode) ?: return null
        return Meaning(
            id = id,
            partOfSpeech = partOfSpeech,
            translation = translation.toEntity(),
            previewUrl = previewUrl?.wrapUrl(),
            imageUrl = imageUrl?.wrapUrl(),
            transcription = transcription,
            soundUrl = soundUrl
        )
    }

    private fun partOfSpeechByCode(code: String): PartOfSpeech? {
        return when (code) {
            "n" -> PartOfSpeech.NOUN
            "v" -> PartOfSpeech.VERB
            "j" -> PartOfSpeech.ADJECTIVE
            "r" -> PartOfSpeech.ADVERB
            "prp" -> PartOfSpeech.PREPOSITION
            "prn" -> PartOfSpeech.PRONOUN
            "crd" -> PartOfSpeech.CARDINAL_NUMBER
            "cjc" -> PartOfSpeech.CONJUNCTION
            "exc" -> PartOfSpeech.INTERJECTION
            "det" -> PartOfSpeech.ARTICLE
            "abb" -> PartOfSpeech.ABBREVIATION
            "x" -> PartOfSpeech.PARTICLE
            "ord" -> PartOfSpeech.ORDINAL_NUMBER
            "md" -> PartOfSpeech.MODAL_VERB
            "ph" -> PartOfSpeech.PHRASE
            "phi" -> PartOfSpeech.IDIOM
            else -> null
        }
    }

}