package ru.mihaylov.words.model.repository.words

import ru.mihaylov.words.entity.MeaningDetails
import ru.mihaylov.words.entity.Word

/**
 * Репозиторий для получения информации о словах
 */
interface WordRepository {

    suspend fun searchWord(word: String) : List<Word>

    suspend fun getMeaningDetails(id: Long) : MeaningDetails

}