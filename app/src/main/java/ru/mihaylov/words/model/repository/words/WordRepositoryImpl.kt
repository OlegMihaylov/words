package ru.mihaylov.words.model.repository.words

import ru.mihaylov.words.entity.MeaningDetails
import ru.mihaylov.words.entity.Word
import ru.mihaylov.words.model.data.server.DictionaryApi

/**
 * Реализация репозитория работы со словарем
 */
class WordRepositoryImpl(private val dictionaryApi: DictionaryApi) : WordRepository {

    override suspend fun searchWord(word: String): List<Word> {
        return dictionaryApi.searchWord(word).map { it.toEntity() }
    }

    override suspend fun getMeaningDetails(id: Long): MeaningDetails {
        return dictionaryApi.getMeaningDetails(id).first().toEntity()
    }

}