package ru.mihaylov.words.model.data.server

import com.google.gson.annotations.SerializedName
import ru.mihaylov.words.entity.Definition

/**
 * Рест представление описания
 */
data class RestDefinition(
    @SerializedName("text") val text: String,
    @SerializedName("soundUrl") val soundUrl: String
) {

    fun toEntity() = Definition(
        text = text,
        soundUrl = soundUrl.wrapUrl()
    )

}