package ru.mihaylov.words.di

import dagger.Module
import dagger.Provides
import ru.mihaylov.words.model.data.server.DictionaryApi
import ru.mihaylov.words.model.repository.words.WordRepository
import ru.mihaylov.words.model.repository.words.WordRepositoryImpl
import ru.mihaylov.words.model.usecase.details.MeaningDetailsUseCase
import ru.mihaylov.words.model.usecase.details.MeaningDetailsUseCaseImpl
import ru.mihaylov.words.model.usecase.search.SearchWordUseCase
import ru.mihaylov.words.model.usecase.search.SearchWordUseCaseImpl
import javax.inject.Singleton

@Module
class ApplicationModule {

    @Provides
    @Singleton
    fun wordRepository(dictionaryApi: DictionaryApi): WordRepository {
        return WordRepositoryImpl(dictionaryApi)
    }

    @Provides
    @Singleton
    fun searchWordUseCase(wordRepository: WordRepository): SearchWordUseCase {
        return SearchWordUseCaseImpl(wordRepository)
    }

    @Provides
    @Singleton
    fun meaningDetailsUseCase(wordRepository: WordRepository): MeaningDetailsUseCase {
        return MeaningDetailsUseCaseImpl(wordRepository)
    }

}