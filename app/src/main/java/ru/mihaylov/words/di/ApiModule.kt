package ru.mihaylov.words.di

import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.mihaylov.words.model.data.server.DictionaryApi
import javax.inject.Singleton


@Module
class ApiModule {

    @Provides
    @Singleton
    fun httpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
        return builder.build()
    }


    @Provides
    @Singleton
    fun dictionaryApi(httpClient: OkHttpClient): DictionaryApi {
        val gson = GsonBuilder().create()
        val retrofitConfig = Retrofit.Builder()
            .client(httpClient)
            .baseUrl("https://dictionary.skyeng.ru/api/public/")
            .addConverterFactory(GsonConverterFactory.create(gson)).build()

        return retrofitConfig.create(DictionaryApi::class.java)
    }


}