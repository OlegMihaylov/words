package ru.mihaylov.words.di

import dagger.Component
import ru.mihaylov.words.presentation.details.MeaningDetailPresenter
import ru.mihaylov.words.presentation.search.SearchPresenter
import ru.mihaylov.words.ui.MainActivity
import javax.inject.Singleton

@Singleton
@Component(modules = [NavigationModule::class, ApiModule::class, ApplicationModule::class])
interface AppComponent {

    fun inject(mainActivity: MainActivity)

    fun inject(searchPresenter: SearchPresenter)

    fun inject(meaningDetailPresenter: MeaningDetailPresenter)

}