package ru.mihaylov.words.entity

/**
 * Слово в словаре
 */
data class Word(
    val id: Long,
    val text: String,
    val meanings: List<Meaning>
)