package ru.mihaylov.words.entity

/**
 * Возможные значения частей речи
 */
enum class PartOfSpeech {
    NOUN,
    VERB,
    ADJECTIVE,
    ADVERB,
    PREPOSITION,
    PRONOUN,
    CARDINAL_NUMBER,
    CONJUNCTION,
    INTERJECTION,
    ARTICLE,
    ABBREVIATION,
    PARTICLE,
    ORDINAL_NUMBER,
    MODAL_VERB,
    PHRASE,
    IDIOM
}