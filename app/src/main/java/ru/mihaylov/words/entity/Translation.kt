package ru.mihaylov.words.entity

/**
 * Перевод
 */
data class Translation(
    val text: String,
    val note: String?
)