package ru.mihaylov.words.entity

/**
 * Детальная информация о значении слова
 */
data class MeaningDetails(
    val id: String,
    val prefix: String?,
    val text: String,
    val translation: Translation,
    val transcription: String,
    val imageList: List<String>,
    val definition: Definition,
    val examples: List<Definition>
)