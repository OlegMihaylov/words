package ru.mihaylov.words.entity

/**
 * Описание
 */
data class Definition(
    val text: String,
    val soundUrl: String
)