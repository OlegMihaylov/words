package ru.mihaylov.words.presentation.details

import kotlinx.coroutines.launch
import moxy.InjectViewState
import moxy.MvpPresenter
import moxy.presenterScope
import ru.mihaylov.words.App
import ru.mihaylov.words.model.usecase.details.MeaningDetailsUseCase
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Презентер отображения детальной информации о значении слова
 */
@InjectViewState
class MeaningDetailPresenter(private val id: Long) : MvpPresenter<MeaningDetailView>() {

    @Inject
    lateinit var meaningDetailsUseCase: MeaningDetailsUseCase

    @Inject
    lateinit var router: Router

    init {
        App.appComponent.inject(this)
    }

    override fun onFirstViewAttach() {
        presenterScope.launch {
            val meaningDetails = meaningDetailsUseCase.getMeaningDetails(id)
            viewState.showMeaningDetails(meaningDetails)
        }
    }

    /**
     * Обработать клик по кнопке "Назад"
     */
    fun onBackPressed() = router.exit()


}