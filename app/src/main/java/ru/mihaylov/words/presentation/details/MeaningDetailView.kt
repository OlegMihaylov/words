package ru.mihaylov.words.presentation.details

import moxy.MvpView
import moxy.viewstate.strategy.alias.AddToEndSingle
import ru.mihaylov.words.entity.MeaningDetails

interface MeaningDetailView : MvpView {

    @AddToEndSingle
    fun showMeaningDetails(meaningDetails: MeaningDetails)
}