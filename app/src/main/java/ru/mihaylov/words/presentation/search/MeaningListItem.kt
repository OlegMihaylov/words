package ru.mihaylov.words.presentation.search

/**
 * Интерфейс записи в списке значений слов
 */
interface MeaningListItem

/**
 * Запись для отображения слова с одним значением
 */
data class SingleMeaningListItem(
    val id: Long,
    val text: String,
    val meaning: String,
    val imageUrl: String?
): MeaningListItem

/**
 * Запись для отображения слова с несколькими значениями
 */
data class MultiMeaningListItem(
    val id: Long,
    val text: String,
    val wordCount: Int,
    val meanings: String,
    val isExpanded: Boolean
): MeaningListItem

