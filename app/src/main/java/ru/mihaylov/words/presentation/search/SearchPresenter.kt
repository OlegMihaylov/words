package ru.mihaylov.words.presentation.search

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import moxy.MvpPresenter
import moxy.presenterScope
import ru.mihaylov.words.App
import ru.mihaylov.words.entity.Word
import ru.mihaylov.words.model.usecase.search.SearchWordUseCase
import ru.mihaylov.words.ui.Screens
import ru.terrakok.cicerone.Router
import javax.inject.Inject

/**
 * Презентер экрана поиска слова
 */
class SearchPresenter : MvpPresenter<SearchView>() {

    @Inject
    lateinit var searchWordUseCase: SearchWordUseCase

    @Inject
    lateinit var router: Router

    private var searchJob: Job? = null
    private var lastSearch: String? = null
    private var searchResult = listOf<Pair<Word, Boolean>>()

    init {
        App.appComponent.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showStartSearchBlock()
    }

    /**
     * Обработать событие поиска слова
     * [search] слово для поиска
     */
    fun onSearchSubmit(search: String) {
        val searchString = normalizeSearchString(search)
        if (searchString == lastSearch) {
            return
        }
        lastSearch = searchString
        searchJob?.cancel()
        if (validateSearchString(searchString)) {
            searchJob = startSearch(searchString)
        } else {
            viewState.showStartSearchBlock()
        }
    }

    private fun normalizeSearchString(search: String) = search.trim()

    private fun validateSearchString(search: String) = search.isNotEmpty()

    private fun startSearch(search: String): Job {
        return presenterScope.launch {
            try {
                val results = searchWordUseCase.searchWord(search)
                if (results.isEmpty()) {
                    viewState.showEmptyResult()
                } else {
                    searchResult = results.map { it to false }
                    viewState.showSearchResults(generateMeaningItems())
                }
            } catch (ignore: CancellationException) {
            } catch (ignore: Exception) {
                viewState.showErrorScreen()
            }
        }
    }

    private fun generateMeaningItems() : List<MeaningListItem> {
        return searchResult.flatMap {
            val result = mutableListOf<MeaningListItem>()
            val word = it.first
            if (it.first.meanings.size == 1) {
                result.add(SingleMeaningListItem(word.meanings.first().id,
                                                 word.text,
                                                 word.meanings.first().translation.text,
                                                 word.meanings.first().imageUrl))
            } else {
                result.add(MultiMeaningListItem(word.id,
                                                word.text,
                                                word.meanings.size,
                                                word.meanings.joinToString { meaning -> meaning.translation.text },
                                                it.second))
                if (it.second) {
                    word.meanings.forEach { meaning ->
                        result.add(SingleMeaningListItem(meaning.id,
                                                         word.text,
                                                         meaning.translation.text,
                                                         meaning.imageUrl))
                    }
                }
            }
            return@flatMap result
        }
    }

    fun onSingleItemClick(item: SingleMeaningListItem) {
        router.navigateTo(Screens.MeaningDetailsScreen(item.id))
    }

    fun onMultiItemClick(item: MultiMeaningListItem) {
        searchResult = searchResult.map { if (it.first.id == item.id) {
                                            return@map Pair(it.first, !it.second)
                                        } else {
                                            return@map it
                                        }
        }
        viewState.showSearchResults(generateMeaningItems())
    }

}