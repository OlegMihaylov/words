package ru.mihaylov.words.presentation.search

import moxy.MvpView
import moxy.viewstate.strategy.alias.SingleState

/**
 * Интерфейс экрана поиска слова
 */
interface SearchView : MvpView {

    /**
     * Отобразить результаты поиска [meaningList]
     */
    @SingleState
    fun showSearchResults(meaningList: List<MeaningListItem>)

    /**
     * Отобразить окно о неудачном поиске
     */
    @SingleState
    fun showEmptyResult()

    /**
     * Отобразить стартовый экран поиска
     */
    @SingleState
    fun showStartSearchBlock()

    /**
     * Отобразить окно с ошибкой поиска
     */
    @SingleState
    fun showErrorScreen()
}