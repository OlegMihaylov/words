package ru.mihaylov.words

import android.app.Application
import ru.mihaylov.words.di.AppComponent
import ru.mihaylov.words.di.DaggerAppComponent

/**
 * Класс приложения
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder().build()
    }

    companion object {
        lateinit var appComponent: AppComponent
    }
}