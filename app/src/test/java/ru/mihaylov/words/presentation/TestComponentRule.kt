package ru.mihaylov.words.presentation

import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import ru.mihaylov.words.App

/**
 * Правило для подстановки тестового компонента в App
 */
class TestComponentRule : TestRule {

    override fun apply(base: Statement?, description: Description?): Statement {
        return object : Statement() {
            @Throws(Throwable::class)
            override fun evaluate() {
                App.appComponent = TestComponent()
                base!!.evaluate()
            }
        }
    }


}