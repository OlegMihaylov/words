package ru.mihaylov.words.presentation

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import ru.mihaylov.words.presentation.search.SearchPresenter
import ru.mihaylov.words.presentation.search.SingleMeaningListItem
import ru.mihaylov.words.presentation.search.`SearchView$$State`

/**
 * Тесты презентера поиска слов
 */
@RunWith(MockitoJUnitRunner::class)
class SearchPresenterTest {

    @Rule
    @JvmField
    val testComponentRule = TestComponentRule()
    private val testDispatcher = TestCoroutineDispatcher()
    @Mock
    private lateinit var searchViewState: `SearchView$$State`

    private lateinit var presenter: SearchPresenter

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        presenter = SearchPresenter()
        presenter.searchWordUseCase = MockSearchWordUseCase()
        presenter.setViewState(searchViewState)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun onEmptySearchTest() {
        presenter.onSearchSubmit("")
        verify(searchViewState).showStartSearchBlock()
    }

    @Test
    fun onWrongSearchTest() = runBlocking {
        presenter.onSearchSubmit("qwert")
        verify(searchViewState).showEmptyResult()
    }

    @Test
    fun onSuccessSearchTest() = runBlocking {
        presenter.onSearchSubmit(MockSearchWordUseCase.TEST_WORD)
        verify(searchViewState).showSearchResults(listOf(
            SingleMeaningListItem(id = 2,
                                text = "cat",
                                meaning = "кошка",
                                imageUrl = "")
        ))
    }
}