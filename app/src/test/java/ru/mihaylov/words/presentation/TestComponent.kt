package ru.mihaylov.words.presentation

import ru.mihaylov.words.di.AppComponent
import ru.mihaylov.words.presentation.details.MeaningDetailPresenter
import ru.mihaylov.words.presentation.search.SearchPresenter
import ru.mihaylov.words.ui.MainActivity

/**
 * Реализация AppComponent для тестов
 */
class TestComponent : AppComponent {

    override fun inject(mainActivity: MainActivity) { }

    override fun inject(searchPresenter: SearchPresenter) { }

    override fun inject(meaningDetailPresenter: MeaningDetailPresenter) { }

}