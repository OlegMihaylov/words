package ru.mihaylov.words.presentation

import ru.mihaylov.words.entity.Meaning
import ru.mihaylov.words.entity.PartOfSpeech
import ru.mihaylov.words.entity.Translation
import ru.mihaylov.words.entity.Word
import ru.mihaylov.words.model.usecase.search.SearchWordUseCase

/**
 * Тестовый сценарий поиска слова
 */
class MockSearchWordUseCase : SearchWordUseCase {

    override suspend fun searchWord(search: String): List<Word> {
        if (search == TEST_WORD) {
            return listOf(Word(
                id = 1,
                text = TEST_WORD,
                meanings = listOf(Meaning(
                    id = 2,
                    partOfSpeech = PartOfSpeech.NOUN,
                    translation = Translation(text = "кошка", note = null),
                    previewUrl = "",
                    imageUrl = "",
                    transcription = "",
                    soundUrl = ""
                ))
            ))
        } else {
            return emptyList()
        }
    }

    companion object {
        const val TEST_WORD = "cat"
    }

}